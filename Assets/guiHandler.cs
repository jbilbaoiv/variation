﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class guiHandler : MonoBehaviour {

	// Use this for initialization
	GameObject [] myObj;
	GameObject targetObj;
	int indexObj;
	string resourceName;
	void Start () {
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (0)) {
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast (ray, out hit, 100.0f)) {//100.0f is distance adjust nalng as needed
				this.resourceName = hit.transform.tag;
				this.targetObj = hit.transform.gameObject;
				populateResource ();
			}			
		}

	}

	void populateResource(){
		myObj = Resources.LoadAll<GameObject>(this.resourceName);
		for(int x = 0; x < myObj.Length; x++) {
			if (targetObj.name == myObj[x].name) {
				indexObj = x;
				break;
			}
		}
	}
	public void nextObj(){
		if (this.resourceName != null || this.targetObj != null) {
			Vector3 objPos = targetObj.transform.position;
			DestroyImmediate (targetObj, true);
			print (myObj.Length);
			if (indexObj == myObj.Length - 1) {
				indexObj = 0;
			} else {
				indexObj++;
			}
			targetObj = Instantiate (myObj [indexObj], objPos, transform.rotation);
		} else {
			print ("No selected Resource");
		}
	}
	public void prevObj(){
		if (this.resourceName != null || this.targetObj != null) {
			Vector3 objPos = targetObj.transform.position;
			DestroyImmediate (targetObj,true);
			print (myObj.Length);
			if (indexObj == 0) {
				indexObj = 3;
			} else {
				indexObj--;
			}
			targetObj = Instantiate (myObj [indexObj], objPos, transform.rotation);
		} else {
			print ("No selected Resource");
		}
	}

}
